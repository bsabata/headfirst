# -*- coding: utf-8 -*-
"""
Created on Wed May 30 21:35:27 2018

@author: Bikash
"""

from distutils.core import setup

setup (
       name       = 'nester',
       version    = '1.2.0',
       py_modules = ['nester'],
       author     = 'bikash',
       author_email = 'bikash@sabata.net',
       url          = 'http://www.sabata.net',
       description  = 'A simple printer of nested lists',
       )