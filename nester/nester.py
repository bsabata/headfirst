# -*- coding: utf-8 -*-
"""
Code from the Chapter 1 of Head First Python. This function prints elements of 
a list recursively - i.e., if there is a list in a list, the inner list is 
recursively printed.

Created on Wed May 30 21:26:19 2018

@author: Bikash
"""

def print_lol(the_list, indent=False, level=0):
    """ print_lol prints the passed list. The list may contain other nested 
    lists recursively. A second argument provides a level of indentation """
    if isinstance(the_list, list): 
        for each_item in the_list:
            print_lol(each_item, indent, level+1)
    else:
        if (indent) :
            for n in range(level):
                print("\t", end='')
        print(the_list)